import React from 'react';
// @note Ini menggunakan Destructuring
// import { btn } from './Button.module.scss'; 
// @note kalau menggunakan dot notation
import style from './Button.module.scss'

/**
 * @Catatan CSS MODULE:
 * CSS module yaitu sebuah CSS file yang semua
 * className dan animasi hanya bagian dari Component
 * lokal nya saja (- Scoped locally by default -)
 * 
 * * implementation:
 * Setelah react running, nama class berubah menjadi
 * .Button_bg-color__18rA{…} dan untuk header
 * .Header_bg-color__94tO{…}.
 * Hal tersebut sesuai dengan 'the following naming convention'
 * yaitu:
 * [filename]\_[classname]\_\_[hash].
 * 
 * jadi dalam kasus ini setelah di build
 * nama class menjadi .Button_btn__18rA.
 * untuk button
 * 
 */


const Button = () => {
  return (
    <>
      <button
        // className={btn} {/* ini menggunakan  Destructuring*/}
        className={style.btn /* ini menggunakan dot notation */}
        onClick={() => alert('I am styled with CSS Modules')}
      >
        I am button 2 - Press Me
      </button>
    </>
  );
};
export default Button;