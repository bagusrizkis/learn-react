import logo from './logo.svg';
/**
 * Di app ini button di style secara global dengan claass btn
 * Tapi di Component Button di style secara Local dengan
 * menerapkan aturan penamaan style "create-react-app"
 * [name].module.css atau [name].module.scss,
 * dimana [name] yaitu nama stylesheet.
 * 
 * Sehingga className tidak terjadi clash
 */
import './App.scss';
import Button from './Button.js'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <button 
            className="btn" 
            onClick={() => alert('I am globally styled')}>
              I am button 1 - Press Me
          </button>
          <Button />
      </header>
    </div>
  );
}

export default App;
